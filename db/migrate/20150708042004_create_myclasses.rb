class CreateMyclasses < ActiveRecord::Migration
  def change
    create_table :myclasses do |t|
      t.string :title
      t.datetime :s_time
      t.datetime :e_time
      t.integer :limit
      t.integer :classroom_id
      t.text :content

      t.timestamps
    end
  end
end
