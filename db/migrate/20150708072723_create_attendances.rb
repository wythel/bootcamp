class CreateAttendances < ActiveRecord::Migration
  def change
    create_table :attendances do |t|
      t.integer :myclass_id
      t.integer :student_id
      t.string :password
      t.integer :score

      t.timestamps
    end
  end
end
