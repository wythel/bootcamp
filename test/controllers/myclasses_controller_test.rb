require 'test_helper'

class MyclassesControllerTest < ActionController::TestCase
  setup do
    @myclass = myclasses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:myclasses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create myclass" do
    assert_difference('Myclass.count') do
      post :create, myclass: { date: @myclass.date, e_time: @myclass.e_time, limit: @myclass.limit, place_id: @myclass.place_id, s_time: @myclass.s_time, title: @myclass.title }
    end

    assert_redirected_to myclass_path(assigns(:myclass))
  end

  test "should show myclass" do
    get :show, id: @myclass
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @myclass
    assert_response :success
  end

  test "should update myclass" do
    patch :update, id: @myclass, myclass: { date: @myclass.date, e_time: @myclass.e_time, limit: @myclass.limit, place_id: @myclass.place_id, s_time: @myclass.s_time, title: @myclass.title }
    assert_redirected_to myclass_path(assigns(:myclass))
  end

  test "should destroy myclass" do
    assert_difference('Myclass.count', -1) do
      delete :destroy, id: @myclass
    end

    assert_redirected_to myclasses_path
  end
end
