Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root 'myclasses#index'
  resources :myclasses, only: [:index]
  get "/myclasses/:id/signup", to: 'myclasses#signup', as: 'signup_myclass'
  get '/myclasses/multi_signup', to: 'myclasses#multi_signup', as: 'multi_signup_myclass'
  get '/myclasses/show/:id', to: 'myclasses#show', as: 'myclass'
  get '/myclasses/showbetween', to: 'myclasses#showbetween', as: 'showbetween_myclass'

  get '/myclasses/:id/score', to: 'myclasses#score', as: 'score_myclass'

  # attendance
  resources :attendances, only: [:create, :show]
  delete "/attendances", to: 'attendances#destroy', as: 'delete_attendance'
  post "/attendances/multi_signup", to: 'attendances#multi_signup', as: 'multi_signup_attendances'
  post '/attendances/score', to: 'attendances#score', as: 'score_attendance'

  # post "/attendance", to: 'attendances#create', as: 'attendances'
  # get "/attendance/:id", to: 'attendances#show', as: 'attendances'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
