ActiveAdmin.register Myclass do
  menu label: "課程"
  config.sort_order = 's_time_desc'

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  permit_params :title, :s_time, :e_time, :content, :classroom_id, :limit

  index do
    column "課程", :title
    column "名額", :limit
    column "已報名" do |myclass|
      myclass.students.count
    end
    column "日期" do |myclass|
      myclass.date_string
    end
    column "時間" do |myclass|
      myclass.time_string
    end
    column "教室" do |myclass|
      myclass.classroom.title
    end
    actions do |myclass|
      link_to "複製", clone_admin_myclass_path(myclass.id), class: "member_link"
    end
  end

  member_action :clone, method: :get do
    myclass = Myclass.find(params[:id])
    @myclass = myclass.dup
    @myclass.content = ''

    if @myclass.save
      redirect_to admin_myclasses_path, notice: "複製了!"
    else
      redirect_to admin_myclasses_path, error: "出了點問題，找志瑋"
    end
  end

  action_item :clone, only: :show do
    link_to '複製', clone_admin_myclass_path(myclass.id)
  end
end
