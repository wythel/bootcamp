ActiveAdmin.register Attendance do
  menu label: "課程參與"
  permit_params :score, :myclass_id, :student_id, :password

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  index do
    column '課程' do |deal|
      deal.myclass.title
    end
    column '日期' do |deal|
      deal.myclass.date_string
    end
    column '時間' do |deal|
      deal.myclass.time_string
    end
    column '姓名' do |deal|
      deal.student.name
    end
    column '刪除碼', :password
    actions
  end

end
