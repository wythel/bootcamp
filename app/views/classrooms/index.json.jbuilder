json.array!(@classrooms) do |classroom|
  json.extract! classroom, :id, :title, :address
  json.url classroom_url(classroom, format: :json)
end
