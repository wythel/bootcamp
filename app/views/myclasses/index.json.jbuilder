json.array!(@myclasses) do |myclass|
  json.extract! myclass, :id, :title, :date, :s_time, :e_time, :limit, :place_id
  json.url myclass_url(myclass, format: :json)
end
