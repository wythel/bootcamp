class Student < ActiveRecord::Base
	has_many :attendances, dependent: :destroy
	has_many :myclasses, through: :attendances

	validates_uniqueness_of :name
	validates_presence_of :name, allow_blank: false, message: '名字不能是空白!'
end
