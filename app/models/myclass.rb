class Myclass < ActiveRecord::Base
	belongs_to :classroom
	has_many :attendances, dependent: :destroy
	has_many :students, through: :attendances

	scope :between, -> (start, over) { where("s_time > ? and s_time < ?", start, over).order("s_time ASC")}

	validates_presence_of :limit

	def time_string
		self.s_time.strftime("%H:%M") + " ~ " + self.e_time.strftime("%H:%M")
	end

	def date_string
		self.s_time.strftime("%Y/%m/%d   %a.")
	end

	def signup_date_string
		date = self.s_time.beginning_of_day - 2.weeks + 1.day
		date.strftime("%m/%d")
	end

	def is_expired
		Time.now > self.e_time
	end

	def is_opened_for_signup
		Time.now > self.s_time.beginning_of_day - 2.weeks + 1.day
	end
end
