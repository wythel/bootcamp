class Attendance < ActiveRecord::Base
	belongs_to :student
	belongs_to :myclass

	validates_uniqueness_of :student_id, :scope => :myclass_id, :message => "你已經登記這堂課了"
	validates_inclusion_of :score, :in => 0..10, :allow_blank => true, :message => "疲勞分數在1~10之間"
end
