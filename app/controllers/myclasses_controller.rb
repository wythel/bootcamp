class MyclassesController < ApplicationController
  before_action :set_myclass, only: [:show, :edit, :update, :destroy, :signup, :score]
  before_action :set_date_time, only: [:signup]

  # GET /myclasses
  # GET /myclasses.json
  def index
    beginning_of_month = Time.zone.now.beginning_of_month
    @this_month = beginning_of_month.strftime("%B")
    @next_month = (beginning_of_month + 1.month).strftime("%B")
    @nnext_month = (beginning_of_month + 2.month).strftime("%B")

    @this_month_classes = Myclass.between(beginning_of_month, beginning_of_month + 1.month)
    @next_month_classes = Myclass.between(beginning_of_month + 1.month, beginning_of_month + 2.month)
    @nnext_month_classes = Myclass.between(beginning_of_month + 2.month, beginning_of_month + 3.month)
  end

  # GET /myclasses/1
  # GET /myclasses/1.json
  def show
  end

  # GET /myclasses/new
  def new
    @myclass = Myclass.new
  end

  # GET /myclasses/1/edit
  def edit
  end

  # POST /myclasses
  # POST /myclasses.json
  def create
    @myclass = Myclass.new(myclass_params)

    respond_to do |format|
      if @myclass.save
        format.html { redirect_to @myclass, notice: 'Myclass was successfully created.' }
        format.json { render :show, status: :created, location: @myclass }
      else
        format.html { render :new }
        format.json { render json: @myclass.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /myclasses/1
  # PATCH/PUT /myclasses/1.json
  def update
    respond_to do |format|
      if @myclass.update(myclass_params)
        format.html { redirect_to @myclass, notice: 'Myclass was successfully updated.' }
        format.json { render :show, status: :ok, location: @myclass }
      else
        format.html { render :edit }
        format.json { render json: @myclass.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /myclasses/1
  # DELETE /myclasses/1.json
  def destroy
    @myclass.destroy
    respond_to do |format|
      format.html { redirect_to myclasses_url, notice: 'Myclass was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /myclasses/1/signup
  def signup
    students = @myclass.students
    @safe = students[0..@myclass.limit-1]
    @back = students[@myclass.limit..-1] ? students[@myclass.limit..-1] : []

    @attend = Attendance.new

    render :signup, status: :success
  end

  # GET /myclasses/multi_signup
  def multi_signup
    @selected_classes = Myclass.find(params[:class_ids])
    @ids = params[:class_ids].join(",")
    render :multi_signup
  end

  def showbetween
    now = Time.zone.now
    @start_date = params[:start_date] ? Time.parse(params[:start_date]) : now - 1.month
    @end_date = params[:end_date] ? Time.parse(params[:end_date]) : now

    @myclasses = Myclass.between(@start_date, @end_date)
    respond_to do |format|
      format.html { render :showbetween }
      format.js
    end
  end

  def score
    # render :score
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_myclass
      @myclass = Myclass.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def myclass_params
      params.require(:myclass).permit(:title, :s_time, :e_time, :limit, :classroom_id, :class_ids)
    end

    def set_date_time
      @date = @myclass.s_time.strftime("%Y/%m/%d")
      @start = @myclass.s_time.strftime("%H:%M")
      @end = @myclass.e_time.strftime("%H:%M")
    end
end
