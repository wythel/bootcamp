class AttendancesController < ApplicationController
  before_action :set_attendance, only: :show

  # POST /attendances
  # POST /attendances.json
  def create
    @attendance = Attendance.new

    student = Student.find_or_create_by(name: attendance_params[:student])
    if student.errors.any?
      @errors = student.errors.full_messages
      render :fail
    elsif attendance_params[:password] == ""
      @errors = ["刪除碼不能是空白"]
      render :fail
    else
      @attendance.student = student
      @attendance.password = attendance_params[:password]
      @myclass = Myclass.find(attendance_params[:myclass_id])

      @attendance.myclass = @myclass
      if @attendance.save
        redirect_to signup_myclass_path(@attendance.myclass), notice: '登記成功'
      else
        @errors = @attendance.errors.full_messages
        render :fail
      end
    end
  end

  def show
  end

  def destroy
    @student = Student.find_by(name: attendance_params[:student])
    @attendance = Attendance.find_by(myclass_id: attendance_params[:myclass_id], student_id: @student.id)

    if attendance_params[:password] != @attendance.password
      redirect_to signup_myclass_path(@attendance.myclass), notice: '刪除碼錯誤'
    elsif @attendance.destroy
      redirect_to signup_myclass_path(@attendance.myclass), notice: '取消成功'
    else
      redirect_to signup_myclass_path(@attendance.myclass), notice: 'Something went wrong'
    end
  end

  def multi_signup
    @student = Student.find_or_create_by(name: attendance_params[:student])
    @classes = Myclass.find(attendance_params[:myclass_ids].split(","))
    @errors = []

    if @student.errors.any?
      @errors.contac(@student.errors.full_messages)
    elsif attendance_params[:password] == ""
      @errors.concat(["刪除碼不能是空白"])
    else
      @classes.each do |myclass|
        attend = Attendance.new()
        attend.myclass = myclass
        attend.student = @student
        attend.password = attendance_params[:password]
        if !attend.save
          @errors.concat(attend.errors.full_messages)
        end
      end
    end

    if @errors.length == 0
      render :success
    else
      render :fail
    end
  end

  def score
    @attendance = Attendance.find_by(student_id: params[:student_id], myclass_id: params[:myclass_id])
    @attendance.score = params[:score]

    if @attendance.save
      redirect_to myclass_path(params[:myclass_id]), notice: "分數已經儲存"
    else
      @errors = @attendance.errors.full_messages
      render :fail
    end

  end

  private
    def set_attendance
      @attendance = Attendance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def attendance_params
      params.require(:attendance).permit(:student, :myclass_id, :password, :myclass_ids)
    end
end
